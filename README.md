# api-uruguay-nvolpe-backend

## Descripción del proyecto
Este proyecto contiene la lógica de BackEnd del curso Tech University - Practitioner Uruguay de Nicolás Volpe.
El objetivo del proyecto es involucrar a los desarrolladores de BBVA Uruguay en tecnologías de vanguardia que son de gran utilidad en el mercado, como es en este caso NodeJS, lenguaje sobre el cual está desarrollado este proyecto.


## Información técnica del proyecto
En la carpeta de entrega de este proyecto se encuentra un documento denominado "Descripción Proyecto - Nicolás Volpe" en donde se podrá encontrar información técnica relevante a este proyecto y su vinculación con el proyecto de frontend.


## Postman
Si se desea invocar este proyecto desde Postman, se deja una colección de peticiones en la siguiente URL:
https://www.getpostman.com/collections/fde49af0d1ecfdc10dd5


## Docker
El proyecto cuenta con un DockerFile que lo expone en el puerto 3000, en caso que se desee crear la imagen de docker.


## Ejecución
El proyecto utiliza una librearía de "compilación" para JavaScript, denominada Backpack. Esta librería permite encontrar errores en el proyecto antes de que el mismo esté ejecutando y le falle al cliente. 
### Comandos para comenzar
Se puede ejecutar el comando "npm run start" que ejecutará el comando backpack y llamará al archivo JavaScript "Index.js" que se encuentra bajo la carpeta "src".

### Uso de nvm
En caso de utilizar el manegador de versiones de node, tal vez sea necesario especificar la versión de node a utilizar antes de correr el proyecto. En dicho caso, se debe tener en cuenta que le proyecto fue desarrollado en node 8. Se indica bajo el comando "nvm use v.{version}"
En el curso se indicó 6 que estaba instalado en la VM, pero por comodidad lo desarrollé en mi servidor local bajo la versión 8 que es la que tenía instalada. Para aclarar que esto no me generó problemas de integración con el frontend bajo ningún momento.