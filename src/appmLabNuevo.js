/** PROPIEDADES DE ENTORNO **/
var port = process.env.PORT || 3000; //Tomara la variable de SO process.env.PORT o si no esta definida toma 3000
var config = require('./config/appconfig');
const URLbase = config._URL_BASE;

/** VARIABLES Y DEPENDENCIAS DE DESARROLLO **/
//REQUEST-JSON
var requestJSON = require('request-json');

//BODY PARSER
var bodyParser = require('body-parser');

//EXPRESS
var express = require('express');
var app = express(); //Permite generar un servidor Web y realizar peticiones
/** Asignacion de propiedades a appExpress **/
app.use(bodyParser.json()); //Uso de bodyParser.json() para parsear en json el body de los request
app.listen(port, () => console.log("Puerto 3000. Apuntando a appmLabNuevo.js")); //Este comando indica por que puerto va a escuchar la appExpress

/*** JWT ***/
/*var jwt = require('jwt-express');
app.use(jwt.init('secret'));
*/
/** CAPA DE SERVICIO **/
var user_service = require('./services/users/user_service');
var account_service = require('./services/accounts/account_service');
var transaction_service = require('./services/transactions/transaction_service');

/** CROS para aceptar peticiones externas **/
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// GET users consumiendo API REST de mLab
app.get( URLbase + 'users',
  function(req, res) {
    let callback = function(status, result) {
      res.status(status);
      res.json(result);
    }
    user_service.getAllUsers(callback);
});

//GET user by userId
app.get( URLbase + 'users/:id',
  function(req, res) {
    let callback = function(status, result) {
      res.status(status);
      res.json(result);
    }
    var userId = req.params.id;
    user_service.getUser(userId, callback);
});


// GET accounts consumiendo API REST de mLab
app.get( URLbase + 'accounts',
  function(req, res) {
    let callback = function(status, result) {
      res.status(status);
      res.json(result);
    }
    account_service.getAllAccounts(callback);
});

//GET account by id
app.get( URLbase + 'accounts/:id',
  function(req, res) {
    let callback = function(status, result) {
      res.status(status);
      res.json(result);
    }
    var accountId = req.params.id;
    account_service.getAccount(accountId, callback);
});

//GET user accounts
app.get( URLbase + 'users/:id/accounts',
  function(req, res) {
    let callback = function(status, result) {
      res.status(status);
      res.json(result);
    }
    account_service.getUserAccounts(req.params.id, callback);
});


//POST agregar user
app.post(URLbase + 'users',
  function(req, res){
    let callback = function(status, result) {
      res.status(status);
      res.json(result);
    }
    user_service.addUser(req.body,callback);
});

//PUT update user
app.put(URLbase + 'users/:id',
  function(req, res){
    let callback = function(status, result) {
      res.status(status);
      res.json(result);
    }
    user_service.updateUser(req.params.id, req.body,callback);
});

//DELETE user
app.delete(URLbase + 'users/:id',
  function(req, res){
    let callback = function(status, result) {
      res.status(status);
      res.json(result);
    }
    user_service.deleteUser(req.params.id,callback);
});

//LOGIN user
app.post(URLbase + 'login',
  function(req,res){
    let callback = function(status, result){
      res.status(status);
      res.json(result);
    }
    user_service.loginUser(req.body, callback);
});

//LOGUOT user
app.post(URLbase + 'logout',
  function(req,res){
    let callback = function(status, result){
      res.status(status);
      res.json(result);
    }
    user_service.logoutUser(req.body, callback);
});

//POST Account
app.post(URLbase + 'accounts', (req, res)=>{
  let callback = function(status, result){
    res.status(status);
    res.json(result);
  }
  account_service.addAccount(req.body, callback);
});

//DELETE Account
app.delete(URLbase + 'accounts/:id', (req, res)=>{
  let callback = function(status, result){
    res.status(status);
    res.json(result);
  }
  let accountId = req.params.id;
  account_service.deleteAccount(accountId, callback);
});

//GET Transactions
app.get(URLbase + 'accounts/:id/transactions', (req, res)=>{
  let callback = function(status, result){
    res.status(status);
    res.json(result);
  }
  let accountId = req.params.id;
  transaction_service.getAccountTransactions(accountId, callback);
});

//POST Transaction
app.post(URLbase + 'transactions', (req, res)=>{
  let callback = function(status, result){
    res.status(status);
    res.json(result);
  }
  let newTransaction = req.body;
  transaction_service.addAccountTransaction(newTransaction, callback);
});

console.log(URLbase);
