var mlab_client_module = require('../../persistance/mlab/client_mlab');
var mlab_client_class = mlab_client_module.client_mlab;
var client_mlab = new mlab_client_class;
var mlab_util = require('../../util/mlabUtils');

class user_service{

  constructor(){
    this.client_mlab = client_mlab;
    this.client_mlab.table = 'user';
  }

  getAllUsers(){
    mlab_util.cleanClient(client_mlab);
    return new Promise((resolve,reject) =>{
        client_mlab.query = undefined;
        client_mlab.get()
        .then(resolve)
        .catch(reject);
      }
    );
  }
 
  getUser(id){
    mlab_util.cleanClient(client_mlab);
    return new Promise((resolve, reject)=>{
      client_mlab.query = {"id":parseInt(id)};
      client_mlab.get()
        .then(user => {
          if(user.length > 0) resolve(user);
          reject({'msg': `User ${id} doesn't exists.`})
        })
        .catch(reject);
    });
  }

  addUser(newUser){
    mlab_util.cleanClient(client_mlab);
    return new Promise((resolve, reject)=>{
      let email = newUser.email;
      client_mlab.query = {"email": email};
      client_mlab.get()
      .then(user => {        
        if(user.length > 0){
          //Si existe usuario con mail
          reject({'msg' : `Already exists an user with ${email} email.`});
        } else {
          //Si no existe usaurio
          client_mlab.sort = {"id":-1};
          client_mlab.limit = 1;
          client_mlab.query = undefined;
          client_mlab.get()
          .then(maxIdUser => {
            let newId = parseInt(maxIdUser[0].id) + 1;
            newUser.id = newId;
            client_mlab.post(newUser)
            .then(resolve)
            .catch(reject);
          })
          .catch(reject);
        }
      })
      .catch(reject);  
    });
  }

  updateUser(userId, userUpdate){
    mlab_util.cleanClient(client_mlab);    
    return new Promise((resolve, reject)=>{
      client_mlab.query = {"$or":[{"id": parseInt(userId)},{"email" : userUpdate.email}]};
      client_mlab.get()
      .then(user => {
        if(user.length>0){          
          if(user[0].id == userUpdate.id && userUpdate.email != user[0].email
            || user[0].id != userUpdate.id){
              reject({'msg' : `You can´t modify user email.`});
          } else {
            const userMerged = {...user[0], ...userUpdate};
            client_mlab.put(userMerged)
            .then(result => {
              if(result.n == 1) resolve(userMerged);
              reject(result);
            })
            .catch(reject);
          }
        } else {
          reject({'msg' : `User ${userUpdate.email} doesn't exists.`});
        }
      })
      .catch(reject);
    });
  }

  deleteUser(userId){
    mlab_util.cleanClient(client_mlab);    
    return new Promise((resolve, reject)=>{
      client_mlab.query = {"id" : parseInt(userId)};
      client_mlab.put([])
      .then(user =>{
        if(user.removed>0) resolve({"msg" : `User ${userId} removed.`})
        reject({"msg" : `User ${userId} doesn´t exists.`});
      })
      .catch(reject);
    });
  }

  loginUser(userCredentials){
    mlab_util.cleanClient(client_mlab);    
    return new Promise((resolve, reject)=>{
      client_mlab.query = {"email":userCredentials.email, "password":userCredentials.password};
      client_mlab.get()
      .then(result => {
        const user = result[0];
        if(user){
          if(!user.logged){
            //Si no estaba loggeado
            var bodyLogin = {"$set": {"logged" : true}};
            client_mlab.put(bodyLogin)
            .then( userLogged =>{              
              const resultPut = {
                "user_id" : user.id,
                "user_email": userCredentials.email,
                "password": userCredentials.password,
                "id_token":"123456",
                "access_token":"access"
              };
              resolve(resultPut);
            })
            .catch(reject);
          } else {
            reject({"msg": `User ${userCredentials.email} is already loggued.`})
          }
        } else {
          reject({"msg":`User ${userCredentials.email} doesn´t exists.`})
        }
      })
      .catch(reject);
    });
  }

  logoutUser(userCredentials){
    mlab_util.cleanClient(client_mlab);    
    return new Promise((resolve, reject)=>{
      client_mlab.query = {"email":userCredentials.email, "password":userCredentials.password, "logged":true};
      client_mlab.get()
      .then(result => {
        const user = result[0];
        if(user){
          const bodyLogout = {"$unset": {"logged":true}};
          client_mlab.put(bodyLogout)
          .then(body => {
            if(body.n>0) resolve({"msg": `User ${userCredentials.email} logout.`});
            reject(body);
          })
          .catch(reject);
        }else{
          reject({"msg": `User ${userCredentials.email} is not logued.`})
        }
      })
      .catch(resolve);
    });
  }
  
}

//Lo exponemos como clase instanciada
module.exports = new user_service();
