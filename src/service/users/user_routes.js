//Basic config
var config = require('../../config/appconfig');
const URLbase = config._URL_BASE;
var bodyParser = require('body-parser');
//Services
var user_service = require('./user_service');
var account_service = require('../accounts/account_service');

module.exports = app =>{
  app.use(bodyParser.json(), function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  // GET users consumiendo API REST de mLab
  app.get( URLbase + 'users', async (req, res)=>{
    try{
      const users = await user_service.getAllUsers();
      res.json(users);
    }catch(message){
      res.json(message);
    }
  });

  //GET user by userId
  app.get( URLbase + 'users/:id', async (req, res)=>{
    try{
      const user = await user_service.getUser(req.params.id);
      res.json(user);
    }catch(message){
      res.status(404).json(message);
    }
  });

  //GET user accounts
  app.get( URLbase + 'users/:id/accounts', async (req, res)=> {
    try{
      const accounts = await account_service.getUserAccounts(req.params.id);
      res.json(accounts);
    }catch(message){
      res.status(400).json(message);
    }
  });

  //POST agregar user
  app.post(URLbase + 'users', async(req, res)=>{
    try{
      const user = await user_service.addUser(req.body);
      res.json(user);
    }catch(message){
      res.status(400).json(message);
    }
  });

  //PUT update user
  app.put(URLbase + 'users', async(req, res) =>{
    try{
      const userUpdated = await user_service.updateUser(req.body.id, req.body);
      res.json(userUpdated);
    }catch(message){
      res.status(400).json(message);
    }
  });

  //DELETE user
  app.delete(URLbase + 'users/:id', async (req, res)=>{
      try{
        const userDeleted = await user_service.deleteUser(req.params.id);
        res.json(userDeleted);
      }catch(message){
        res.status(400).json(message);
      }
  });

  //LOGIN user
  app.post(URLbase + 'login', async (req,res)=>{
      try{
        const userLogged = await user_service.loginUser(req.body);
        res.json(userLogged);
      }catch(message){
        res.status(400).json(message);
      }
  });

  //LOGUOT user
  app.post(URLbase + 'logout', async(req,res)=>{
      try{
        const userLogout = await user_service.logoutUser(req.body);
        res.json(userLogout);
      }catch(message){
        res.status(400).json(message);
      }
  });
}
