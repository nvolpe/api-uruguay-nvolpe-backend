//Basic config
var config = require('../../config/appconfig');
const URLbase = config._URL_BASE;
var bodyParser = require('body-parser');
//Services
var account_service = require('./account_service');
var transaction_service = require('../transactions/transaction_service');

module.exports = app =>{
  app.use(bodyParser.json(), function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  //GET account by id
  app.get( URLbase + 'accounts/:id', async (req, res) => {
      try{
        const account = await account_service.getAccount(req.params.id);
        res.json(account);
      }catch(message){
        res.status(404).json(message);
      }
  });

  //POST Account
  app.post(URLbase + 'accounts', async (req, res)=>{
    try{
      const account = await account_service.addAccount(req.body);
      res.json(account);
    }catch(message){
      res.status(400).json(message);
    }
  });

  //DELETE Account
  app.delete(URLbase + 'accounts/:id', async (req, res)=>{
    try{
      const deletedAccount = await account_service.deleteAccount(req.params.id);
      res.json(deletedAccount);
    }catch(message){
      res.status(404).json(message);
    }
  });

  //GET account transactions
  app.get(URLbase + 'accounts/:id/transactions', async (req, res)=>{
    try{
      const transactions = await transaction_service.getAccountTransactions(req.params.id, req.query);
      res.json(transactions);
    }catch(message){
      res.status(400).json(message)
    }
  });
}
