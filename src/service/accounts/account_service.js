var mlab_client_module = require('../../persistance/mlab/client_mlab');
var mlab_client_class = mlab_client_module.client_mlab;
var client_mlab = new mlab_client_class;
var mlab_util = require('../../util/mlabUtils');

/*
--Tipos de cuentas:
1: Cuenta corriente
2: Caja de ahorro
3: Cuenta cajero

--Monedas:
1: Dollar
2: Euro
3: Peso
*/

class account_service{
  constructor(){
    this.client_mlab = client_mlab;
    this.client_mlab.table = 'account';
  }

  getAccount(id){
    mlab_util.cleanClient(client_mlab);    
    return new Promise((resolve, reject)=>{
      this.client_mlab.filters = {"_id" : 0};
      client_mlab.query = {"id":parseInt(id)};
      client_mlab.get()
      .then(result => {
        if(result.length> 0) resolve(result);
        reject({'msg': `Account ${id} doesn´t exists.`});
      })
      .catch(reject);  
    });
  }


  getUserAccounts(user_id){
    mlab_util.cleanClient(client_mlab);
    return new Promise((resolve, reject)=>{
      client_mlab.query = {"id_user":parseInt(user_id)};
      this.client_mlab.filters = {"_id" : 0};      
      client_mlab.get()
      .then(resolve)
      .catch(reject);
    });
  }

  addAccount(newAccount){
    mlab_util.cleanClient(client_mlab);    
    return new Promise((resolve, reject)=>{
      client_mlab.sort = {"id":-1};
      client_mlab.limit = 1;
      client_mlab.get()
      .then(result => {
        let newId = parseInt(result[0].id) + 1;
        newAccount.id = newId;
        client_mlab.post(newAccount)
        .then(resolve)
        .catch(reject);
      })
      .catch(reject);
    });
  }

  deleteAccount(accountId){
    mlab_util.cleanClient(client_mlab);    
    return new Promise((resolve, reject)=>{
      client_mlab.query = {"id": parseInt(accountId)};
      let bodyDeleted = {"$set": {"deleted" : true}};
      client_mlab.put(bodyDeleted)
      .then(resolve)
      .catch(reject);
    });
  }
  
  modifyBalance(accountId, amount){
    console.log("BALANCE A MODIF ACOUNT: " + accountId + " - " + amount);
    
    mlab_util.cleanClient(client_mlab);    
    return new Promise((resolve, reject)=>{
      client_mlab.query = {"id": parseInt(accountId)};
      let bodyInc = {"$inc": {"balance" : amount}};
      client_mlab.put(bodyInc)
      .then(resolve)
      .catch(reject);
    });
  }
}

//Lo exponemos como new clase
module.exports = new account_service();
