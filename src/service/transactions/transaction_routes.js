//Basic config
var config = require('../../config/appconfig');
const URLbase = config._URL_BASE;
var bodyParser = require('body-parser');
//Services
var transaction_service = require('./transaction_service');

module.exports = app => {
  app.use(bodyParser.json(), function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
  //GET Account Transactions
  app.get(URLbase + 'accounts/:id/transactions', async (req, res)=>{
    try{
      const transactions = await transaction_service.getAccountTransactions(req.params.id);
      res.json(transactions);
    }catch(message){
      res.status(400).json(message);
    }
  });

  //GET Transactions
  app.get(URLbase + 'transactions', async (req, res)=>{
    try{
      const transactions = await transaction_service.getTransactions();
      res.json(transactions);
    }catch(message){
      res.status(400).json(message);
    }
  });

  //POST Transaction
  app.post(URLbase + 'transactions', async (req, res)=>{
    try{
      const transaction = await transaction_service.addAccountTransaction(req.body);
      res.json(transaction);
    }catch(message){
      res.status(400).json(message);
    }
  });
}
