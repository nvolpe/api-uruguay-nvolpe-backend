var mlab_client_module = require('../../persistance/mlab/client_mlab');
var mlab_client_class = mlab_client_module.client_mlab;
var client_mlab = new mlab_client_class;
var currency_converter_module = require('../../externalServices/currencyconverter');
var currency_converter_class = currency_converter_module.client_curr_conv;
var client_cconv = new currency_converter_class;
var datetime = require('node-datetime');
var mlab_util = require('../../util/mlabUtils');
var format = require('date-fns/format');
var isWithinRange = require('date-fns/is_within_range');
var addDays = require('date-fns/add_days');
var account_service = require('../accounts/account_service');

/*
Types:
1- Cuenta propia
2- Cuenta tercero
*/

class transaction_service{

  constructor(){
    this.client_mlab = client_mlab;
    this.client_mlab.table = 'transaction';
  }

  getTransactions(){
    mlab_util.cleanClient(client_mlab);
    return new Promise((resolve, reject) => {
      client_mlab.sort = {"date": -1};
      client_mlab.get()
      .then(resolve)
      .catch(reject);
    });
  }

  getAccountTransactions(accountId, filters){
    mlab_util.cleanClient(client_mlab);
    return new Promise((resolve, reject) => {
      client_mlab.query = {"$or" : [{"id_sender_account": parseInt(accountId)}, {"id_receiver_account" : parseInt(accountId)}]};
      client_mlab.sort = {"date": -1};
      client_mlab.get()
      .then(transactions => {
        if(filters.startDate){
          const type = filters.type? filters.type : 0;           
          //Tratamos filtros
          const start= format(filters.startDate, 'YYYY-MM-DD HH:mm:ss');
          const end = format(addDays(filters.endDate, 1), 'YYYY-MM-DD HH:mm:ss');
          let result = [];
          transactions.forEach(element => {
            const elementDate = format(element.date, 'YYYY-MM-DD HH:mm:ss');            
            if(isWithinRange(elementDate, start, end)
              && (type === 0 || element.type == type)){
              result.push(element);
            }
          });
          resolve(result);
        } else {
          resolve(transactions);
        }
      })
      .catch(reject);
    });
  }
  
  addAccountTransaction(newTransaction){
    mlab_util.cleanClient(client_mlab);    
    return new Promise((resolve, reject) => {
      //Primer control fundamental --> que la cuentas sean diferentes
      if(newTransaction.id_receiver_account === newTransaction.id_sender_account) reject({msg : "Sender and receiver are the same."});
      //En caso contrario continuamos
      client_mlab.sort = {"id": -1};
      client_mlab.limit = 1;
      client_mlab.get()
      .then(result => {
        const newId = parseInt(result[0].id) + 1;
        const now = datetime.create();
        const nowFormatted = now.format('Y-m-d H:M:S');
        newTransaction.id = newId;
        newTransaction.date = nowFormatted;
        //Obtenmeos info de sender y receiver
        account_service.getAccount(newTransaction.id_sender_account)
        .then(responseSender =>{
          let sender = responseSender[0];
          account_service.getAccount(newTransaction.id_receiver_account)
          .then(responseReceiver =>{
            let receiver = responseReceiver[0];
            /***** RESOLVEMOS TIPO DE TRANSACCION ****/
            if(sender.id_user === receiver.id_user){
              //Tipo 1 --> Own transfer
              console.log("ES DE TIPO OWNER");
              newTransaction.type = 1;
            } else {
              //Tipo 2 --> Internal transfer
              console.log("ES DE TIPO INTERNAL");
              newTransaction.type = 2;
            }
            /*** RESOLVEMOS IMPORTE ***/
            //La moneda va a ser siempre la del destino!
            newTransaction.currency = receiver.currency;
            if(sender.currency != receiver.currency){              
              client_cconv.from = sender.currency;
              client_cconv.to = receiver.currency;

              client_cconv.getCurrencyConvertion().then(value => {                
                let newAmount = newTransaction.amount * value;
                newTransaction.amount = newAmount;
                newTransaction.exchangeRate = value;

                console.log("VAMOS A TRANSFERIR CONVERTIDO: " + newTransaction.currency + " - " + newTransaction.amount);
                this.completeTransaction(newTransaction)
                .then(resolve)
                .catch(reject);
              })
              .catch(reject);
            } else {
              console.log("VAMOS A TRANSFERIR SIN CONVERTIR: " + newTransaction.currency + " - " + newTransaction.amount);
              newTransaction.exchangeRate = 1;              
              this.completeTransaction(newTransaction)
              .then(resolve)
              .catch(reject);
            }
          });
        });
      })
      .catch(reject);
    });
  }

  completeTransaction(finalTransaction){
    console.log("ENTRE A COMPLETAR con TRAN: " + JSON.stringify(finalTransaction));
    
    return new Promise((resolve, reject)=>{
      client_mlab.post(finalTransaction)
            .then(transaction =>{
              let moneyToDebt = (-1 * ( finalTransaction.amount / finalTransaction.exchangeRate));
              console.log("MONEY TO DEBT: " + moneyToDebt);
              
              //Quitamos dinero al sender
              account_service.modifyBalance(finalTransaction.id_sender_account, moneyToDebt)
              .then(
                //Damos dinero al receiver
                account_service.modifyBalance(finalTransaction.id_receiver_account, finalTransaction.amount)
                .then(resolve(transaction))
                .catch(error =>{
                  account_service.modifyBalance(finalTransaction.id_sender_account, -1* moneyToDebt)
                  .then(client_mlab.delete(finalTransaction.id)
                    .then(reject({msg : `Error transferring money: ${error}`})))
                }))
              .catch(reject);
            })
            .catch(reject);
    });
  }

}

//Lo exponemos como clase instanciada
module.exports = new transaction_service();
