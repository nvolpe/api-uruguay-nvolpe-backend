
/********* VARIABLES DE ENTORNO *********/
//Generales
let version = "v1";
let _URL_BASE = "/api-uruguay/" + version + "/";

//MLAB
var _URL_MLAB = "https://api.mlab.com/api/1/databases";
var _REPO_MLAB = "/techubduruguay25";
var _URL_MLAB_BASE = _URL_MLAB + _REPO_MLAB + "/collections/";
var _API_KEY_MLAB = "_3ukuLHO7MOEjy80bhS4Pd-7CpG6dvQT";

//CURRENCY CONVERTER
var _URL_CURRENCY_CONVERTER = "http://free.currencyconverterapi.com/api/v6/convert"

module.exports = {
    _URL_BASE: _URL_BASE,
    _URL_MLAB_BASE: _URL_MLAB_BASE,
    _API_KEY_MLAB: _API_KEY_MLAB,
    _URL_CURRENCY_CONVERTER: _URL_CURRENCY_CONVERTER
}
