var config = require('../../config/appconfig');
var _URL_MLAB_BASE = config._URL_MLAB_BASE;
var _API_KEY_MLAB = config._API_KEY_MLAB;
var requestJSON = require('request-json');

class client_mlab{
  constructor(){
    this.table;
    this.query;
    this.filters;
    this.sort;
    this.limit;
  }

  //Arma la url de mlab para crear cliente
  getmlaburl(){
    var apikey = "&apiKey=" + _API_KEY_MLAB;
    var retorno = _URL_MLAB_BASE + this.table + '?';
    //Si tengo querys seteados
    if(this.query != undefined){
      retorno += '&q=' + JSON.stringify(this.query);
    }
    //Si tengo filtros seteados
    if(this.filters != undefined){
      retorno += '&f=' + JSON.stringify(this.filters);
    }
    //Si tengo ordenes seteados
    if(this.sort != undefined){
      retorno += '&s=' + JSON.stringify(this.sort);
    }
    //Si tengo limites seteados
    if(this.limit != undefined){
      retorno += '&l=' + this.limit;
    }
    return retorno + apikey;
  }

  //PETICION GET
  get(){
    return new Promise((resolve, reject) => {
      let _CLIENT_URL = this.getmlaburl();
      var mlab_client = requestJSON.createClient(_CLIENT_URL);
      mlab_client.get('', (err, mLabResponse, body) => {
        if(!err) resolve(body);
        reject(mLabResponse); 
      });
    });
  }

  //PETICION POST
  post(newItem){
    return new Promise((resolve, reject)=>{
      let _CLIENT_URL = this.getmlaburl();
      var mlab_client = requestJSON.createClient(_CLIENT_URL);
      mlab_client.post('', newItem, (err, mLabResponse, body) => {
       if(!err) resolve(body);
       reject(mLabResponse); 
      });    
    });
  }

  //PETICION PUT
  put(itemToUpdate){
    return new Promise((resolve, reject)=>{
      let _CLIENT_URL = this.getmlaburl();
      var mlab_client = requestJSON.createClient(_CLIENT_URL);
      mlab_client.put('', itemToUpdate, (err, mLabResponse, body) => {
        if(!err) resolve(body);
        reject(mLabResponse); 
      });
    });
  }

  //PETICION DELETE
  delete(itemToUpdate){
    return new Promise((resolve, reject)=>{
      let _CLIENT_URL = this.getmlaburl();
      var mlab_client = requestJSON.createClient(_CLIENT_URL);
      mlab_client.put('', itemToUpdate, (err, mLabResponse, body) => {
        if(!err) resolve(body);
        reject(mLabResponse);
      });
    });
  }
}

module.exports = {
    client_mlab: client_mlab
}
