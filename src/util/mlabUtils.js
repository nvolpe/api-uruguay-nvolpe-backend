module.exports = {

    cleanClient : function(client_mlab){
        client_mlab.query = undefined;
        client_mlab.sort = undefined;
        client_mlab.limit = undefined;
        client_mlab.filters = {"_id" : 0};
      }
}