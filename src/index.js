var port = process.env.PORT || 3000; //Tomara la variable de SO process.env.PORT o si no esta definida toma 3000
//EXPRESS
var express = require('express');
var app = express();

//ROUTES
var user_routes = require('./service/users/user_routes');
var account_routes = require('./service/accounts/account_routes');
var transaction_routes = require('./service/transactions/transaction_routes');


//Expose routes and listen
user_routes(app);
account_routes(app);
transaction_routes(app);


app.listen(port, () => console.log("Puerto 3000. Apuntando a app.js"));
