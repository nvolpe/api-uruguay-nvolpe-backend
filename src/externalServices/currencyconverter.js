var config = require('../config/appconfig');
var _URL_CCONV_BASE = config._URL_CURRENCY_CONVERTER;
var requestJSON = require('request-json');

class client_curr_conv{
    constructor(){
        this.from;
        this.to;
        this.currFrom;
        this.currTo;
    }

    getCurrConvUrl(){
        this.currFrom = this.from === 1 ? "USD" : this.from === 2? "EUR" : "UYU";
        this.currTo = this.to === 1 ? "USD" : this.to === 2? "EUR" : "UYU";
        return _URL_CCONV_BASE + `?q=${this.currFrom}_${this.currTo}`;
    }


    getCurrencyConvertion(){
        return new Promise((resolve, reject)=>{
            let url = this.getCurrConvUrl();            
            let client = requestJSON.createClient(url);
            let conv = `${this.currFrom}_${this.currTo}`;
            client.get('', (err, resp, body)=>{
                !err? resolve(body.results[conv].val) : reject;
            });
        });
    }
}

module.exports = {
    client_curr_conv: client_curr_conv
}
